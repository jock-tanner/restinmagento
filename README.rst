Restinmagento
==========================

Restinmagento is a client library for the Magento REST API.

It aims to provide a pythonic way to interact with magento REST resources through:

    * a straight forward low level API exposing standard HTTP methods on resources.
    * a higher level object oriented API (ORM or object database style)
    * some command line tools to help obtaining acces tokens from a Magento server.


Restinmagento uses `semantic versioning <http://semver.org/>`_. As a consequence the API should be considered unstable as long as version 1.0 is not reached. That being said, the API is already pretty stable and tested, so give it a shot !

Documentation
-------------------------

Documentation is available here: `http://jmsinfor.com/projects/restinmagento/ <http://jmsinfor.com/projects/restinmagento/>`_.


License
--------------------------

Restinmagento is offered under the `MIT Licence <http://opensource.org/licenses/MIT>`_.

Sponsor
-------------------------

Restinmagento is heavily sponsored by `JMS Informatique <http://www.jmsinfor.com/>`_ .


