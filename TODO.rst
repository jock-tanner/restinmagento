Restinmagento TODOs
=============

- implemet tracking of dirty attributes to optimize save() in Model.

- Documentation  on subclasses of instances.

- Create a FAQ

- Remove global configuration of backend, instead get models from a backend.

- Hide the Oauth object in high level API
