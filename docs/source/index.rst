.. index:

===================================================================
Restinmagento: a client library for the magento REST API
===================================================================

Restinmagento is a client library that communicates with the Magento REST API.

Its provides a low level API to interact with the Magento REST resources through standard HTTP methods, and a high level API which lets let you manipulates resources in an 'ORM' fashion.


Here is a simple script that uses Restinmagento's low level API to retrieve the list of products data in a dictionnary:

.. code-block:: python
    :linenos:

    from restinmagento.oauth import OAuth
    from restinmagento.resource import Resource

    oauth = OAuth(client_key=u'yourclientkey',
        client_secret=u'yourclientsecret',
        resource_owner_key=u'yourresourcekey',
        resource_owner_secret=u'yourresourcesecret')

    resource  = Resource(u'http://yourmagentohost/api/rest/products', oauth)
    products_dict = resource.get()

Magento uses Oauth to authenticate third party applications. You need to specify some oauth tokens to enable
communication beetween your Python script and your Magento server. Checkout :ref:`oauth_tokens` to learn how to obtain these tokens.

    
Here is a another script that uses Restinmagento's high level API to create a new product and save it to your Magento server.

.. code-block:: python
    :linenos:

    from restinmagento.backend import set_default_backend
    from restinmagento.model import Product
    from restinmagento.oauth import OAuth

    oauth = OAuth(client_key=u'yourclientkey',
        client_secret=u'yourclientsecret',
        resource_owner_key=u'yourresourcekey',
        resource_owner_secret=u'yourresourcesecret')
    set_default_backend(url=u'http://yourmagentohost/api/rest', oauth=oauth)

    product_data = dict(type_id='simple', attribute_set_id='4',
        sku='prodsku', name='prodname', price='10',
        weight='1', status='1', visibility='2',
        tax_class_id='2', description='product description',
        short_description='short description')
   
    product = Product(data=product_data)
    product.save()

We specify in ``product_data`` the mandatory data that Magento expects when you create a new product, instantiate a ``Product`` objet with ``product_data`` and use ``save`` method to store it in our Magento server. Check out `Magento documentation <http://www.magentocommerce.com/api/rest/Resources/Products/products.html#RESTAPI-Resource-Products-HTTPMethod-POST-products>`_ to find out which parameters to use when creating a product.  
  


    
Narrative documentation
==============================================================

.. toctree::
   :maxdepth: 1

   narr/tokens.rst
   narr/resource.rst
   narr/orm.rst


API Reference
==============================================================

.. toctree::
   :maxdepth: 1

   api/collectionmanager.rst
   api/model.rst
   api/operators.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

