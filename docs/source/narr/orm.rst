ORM like API
-----------------------------------

Restinmagento enable manipulating Magento resources in an ORM fashion. This API is strongly inspired by the Django ORM API.


Creating an object
====================================

.. code-block:: python
    :linenos:

    from restinmagento.backend import set_default_backend
    from restinmagento.model import Product
    from restinmagento.oauth import OAuth

    oauth = OAuth(client_key=u'yourclientkey',
        client_secret=u'yourclientsecret',
        resource_owner_key=u'yourresourcekey',
        resource_owner_secret=u'yourresourcesecret')
    set_default_backend(url=u'http://yourmagentohost/api/rest', oauth=oauth)

    product_data = dict(type_id='simple', attribute_set_id='4',
        sku='prodsku', name='prodname', price='10',
        weight='1', status='1', visibility='2',
        tax_class_id='2', description='product description',
        short_description='short description')
   
    product = Product(data=product_data)
    product.save()


In the script above create an oauth token, and configure a `backend` with the root URL of a Magento server and the token as parameters. You just specify here what is the Magento server you want to work with and how to authenticate with it. Behind the scene a backend object will be used by the librairy any time a a call to the Magento server is necessary, but you don't have to worry about it any more once you configured it.

Then we create a dictionnary of data, that will be used during the Product initialization. These are the mandatory attributes that the Magento REST API requires. Checkout the `official documentation <http://www.magentocommerce.com/api/rest/Resources/Products/products.html#RESTAPI-Resource-Products-HTTPMethod-POST-products>`_ to know which are these attributes and which values are authorized. 

All we have to do to create a new product is to instanciate a `Product` object with these attributes and call the `save` method.

.. code-block:: python
    :linenos:
   
    product = Product(**product_data)
    product.save()

The `save` methods perform a call to the Magento REST server and ensure that your `product` is saved in it.

Primary key
*************************

A call to the `save` method automatically populates the `pk` attribute on the object. This is the primary key created by the Magento server for your newly created object.

Modifying and deleting
====================================

If you want to modify this existing object, just modify it as any regular python object and call the `save` method to persit your changes. for example changing the name of this product requires the following step:

.. code-block:: python
    :linenos:
   
    product.name = u'new name'
    product.save()

Deleting an object takes a call to the `delete` method.

.. code-block:: python
    :linenos:
   
    product.delete()


Working with objects collection
====================================

A must have in an ORM like API is the ability to request from the server some specific objects.
Restinmagento provides a `CollectionManager` object for performing such tasks.
Suppose you server contains a `product` object whose primary key is 1. You could get it from your server like this:

.. code-block:: python
    :linenos:
   
    product = Product.objects.get(1)

The `Product.objects` call return a CollectionManager for the Product objects. The `get` method takes a primary key as argument and returns the object with the specified primary key.

Another basic need is to get object from your collections. To do so use the `all` method:

.. code-block:: python
    :linenos:
   
    products = Product.objects.all()

The `products` object is now a list containing every product instance stored in your Magento server.

What about filtering some products with apsecific criterion ? Use the `filter` method.

.. code-block:: python
    :linenos:
   
    products = Product.objects.filter(name='myproductname')

You can chain the `filter` method call. This will return the objects matching every criteria you specified (implicit AND operator).

.. code-block:: python
    :linenos:
   
    products = Product.objects.filter(name='myproductname').filter(sku='somesku')

For more complex filtering operations, Restinmagento comes with some handy operators:

.. code-block:: python
    :linenos:
    
    from restinmagento.operators import lt_
    products = Product.objects.filter(lt_('name', 'dummy'))

This will return every products whose `name` is `lesser than 'dummy'`. See :ref:`operators_module` for a full list of available filtering operators.

For now the `filter` method allows only to get some product whose attributes values match exactly what you specified as parameter of the `filter` method. Nonetheless Magento REST API supports a lot more `filtering operation  <http://www.magentocommerce.com/api/rest/get_filters.html>`_.
To use some filtering operations that are not implemented in Restinmagento, use the `raw_filter` method:

.. code-block:: python
    :linenos:
   
    criterion = {'filter[1][attribute]': 'price',
                 'filter[1][gt]': '10',}
    products = Product.objects.raw_filter(criterion)

This script fetches every products whose price is greater than 10.