
Using resources
======================

The Resource object is the enty point to the low level API. A Resource is defined by its URL, and let you use standard HTTP methods on these URLS. In the following we will use the `product` resource defined in the `Magento REST API <http://www.magentocommerce.com/api/rest/Resources/Products/products.html>`_.

Credits
---------------------------------

The Resource object is thin wrapper around the `delicious requests library <http://docs.python-requests.org/>`_ and its `Oauth add-on <https://github.com/requests/requests-oauthlib>`_. Actually a `
Resource` does not add any features compare to this library. The `Resource` object goal is just to  provide a single entry point to the Magento REST API, and to call the methods of the API with a fewer lines of code than if you use  directly `requests <http://docs.python-requests.org/>`_.


The get method
----------------------------------

.. code-block:: python
    :linenos:

    from restinmagento.oauth import OAuth
    from restinmagento.resource import Resource

    oauth = OAuth(client_key=u'yourclientkey',
        client_secret=u'yourclientsecret',
        resource_owner_key=u'yourresourcekey',
        resource_owner_secret=u'yourresourcesecret')

    resource  = Resource(u'http://yourmagentohost/api/rest/products', oauth)
    response = resource.get()
    if response.status_code == 200:
        products = response.json()

The above script creates a `Resource` object, with the URL of the Magento `products` resource and a Oauth token. Then it sends a standard GET request to this URL, using the `get` method on the resource:

.. code-block:: python
    :linenos:

    response = resource.get()
    
The `response` object wraps the HTTP response, and contains several usefull informations. For example `status_code` contains the HTTP reponse code. 200 means OK.

.. code-block:: python
    :linenos:

    if response.status_code == 200:
        products = response.json()

The response is in `200` status_code, the Magento REST API should have populated the response with some json string. Therefore use this string to create a python object (a dictionnary or a list depending on what Magento resource you hit), by calling the `json` method.

In our case, the Magento product resource sends a dictionnary like json string, so the `products` variable is a standard python dictionnary.

Using parameters
**********************************

The Magento REST API enables using `paramaters <http://www.magentocommerce.com/api/rest/get_filters.html>`_ when sending a GET request.

The above script does not use any paramters. By default the Magento REST API shoul have returned the first `page` of products, with 10 products in it. What if we wanted the second page of products ? We could add a `params` dictionnary to the `get` method:

.. code-block:: python
    :linenos:

    params = dict(page=2)
    response = resource.get(params=params)

What if we wanted to get 20 products in single call ? Use a `limit` key in the params dictionnary:

.. code-block:: python
    :linenos:

    params = dict(limit=20)
    response = resource.get(params=params)

And of course you can mix several parameters in the same call, for exmaple if you want the products of the second page ordered by name, you could use the following code:

.. code-block:: python
    :linenos:

    params = dict(page=2, order='name')
    response = resource.get(params=params)

The post method
----------------------------------

The post method enbales sending POST requests to the resource. For example the Magento REST API the creation of a new product through a POST request:

.. code-block:: python
    :linenos:
    
    product_data = dict(type_id='simple', attribute_set_id='4',
            sku='prodsku', name='prodname', price='10',
            weight='1', 'status'='1', visibility='2',
            tax_class_id='2', description='product description',
            short_description='short description', google_product_type='1')
    response = resource.post(data=product_data)
    new_product_url = response.headers['location']

We specify in `product_data` the required attributes requested by the `Magento API <http://www.magentocommerce.com/api/rest/Resources/Products/products.html#RESTAPI-Resource-Products-HTTPMethod-POST-products>`_. We the then call the `post` method on our Resource obj. Eventhough it is undocummented in the MAgento API, you will realize that the response to  such a request contains the location of the newly created resource in its `location` header.

The put method
--------------------------------------

Suppose we now wanted to updated this newly created product. Magento allows it by sending a PUT request to the resource URL.

.. code-block:: python
    :linenos:
    
    resource  = Resource(u'http://yourmagentohost' + new_product_url, oauth)
    product_data = dict(name='newname')
    response = resource.put(data=product_data)
    
We first create the resource object corresponding to the new product (using the `new_product_url` we gathered in the previous step).
In the above script, we assume we only want to change the product name, and therefore `name` is the only parameters we send. The Magento API does not return any usefull information in the case of a product update, so we don't do anything with the `response` object.

The delete method
----------------------------------------------

Finally if we wanted to delete that resource, we could do so by sending a DELETE request to it.


.. code-block:: python
    :linenos:
    
    response = resource.delete()

Once again the Magento API does not return any usefull information in the case of a product update, so we don't do anything with the `response` object.