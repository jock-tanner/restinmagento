.. _oauth_tokens:

Obtaining OAuth tokens
======================

Magento REST API uses 3-legged OAuth 1.0a protocol to authenticate the application to access the Magento service. To put it simple you need to obtain some tokens from your Magento server, and use these token in your application. Restinmagento provides some command line scripts that will help you obtain these tokens.

**Disclaimer**: the following material is not a general introduction to OAuth. For such material, check for example http://hueniverse.com/oauth/guide/, or use your favorite search engine.

For some specific Magento's Oauth materials, check `the official doc <http://www.magentocommerce.com/api/rest/authentication/oauth_authentication.html>`_ or `this tutorial by Ashley Schroder <http://www.aschroder.com/2012/04/introduction-to-the-magento-rest-apis-with-oauth-in-version-1-7/>`_ .


Client token
----------------------------

The client token will authenticate your application to your Magento server. To obtain this token go your Magent admin panel, in **System -> Web Services -> REST OAuth Consumers**. Create a new consumer (you just have to fill the `name` field). Fields **Key** and **Secret** are prepopulated, this is your client token, we will need it in the next steps.


Temporary token
-----------------------------
We will use a command line script for this one. In a shell environnement where Restinmagento is installed, do:

.. code-block:: tcsh
    :linenos:

    $ rim-tmptoken --url=http://yourmagentoserver/oauth/initiate --clientkey=yourclientkey --clientsecret=yourclientsecret

The `clientkey` and `clientsecret` are the one you got from the previous step. If everything goes well you should see this:

.. code-block:: tcsh
    :linenos:

    Obtaining a temporary token.
    Temporary key: somecomplicatedtemporarykey
    Temporary secret: somecomplicatedtemporarysecret


.. warning:: If your Magento server is configured to use the store feature, your requests to obtain tokens might return a HTTP 404 error code. In that case append a store code to the url you are using to obtain tokens.

    For example if configured a store to run with the `en` code, you might need to use http://yourmagentoserver/en/oauth/initiate as URL so obtain a temporary token. This remark also applies to the following steps.


Authorize your application and obtain a verifier
-----------------------------------------------------------------

The next step is to allow your application to access the Magento resources, and to get a verifier. To do so use your favorite web browser and visit the following URL: http://yourmagentoserver/admin/oauth_authorize?oauth_token=somecomplicatedtemporarykey

Make sure you fill the  `oauth_token` parameter with the temporary key you got from the previous step. You should be invited to log in and then to authorize the application to access Magento resources.

After authorizing your application, your browser will be redirected to a web page that does not exist, this is a 'normal' behaviour. 

**But don't close your browser !**

The URL where your browser is redirected sould be similar to this: 

`http://yourmagentoserver/oauth/initiate?oauth_token=somecomplicatedtemporarykey&oauth_verifier=somecomplicatedverifier`. 

**Please make sure to note somewhere the verifier parameter of the URL**, you will need it in the final step.

Resource owner token
------------------------------------------------------------------------

This is the final step in which you will get a resource owner token. This token will allow your scripts to connect to the Magento REST API. I odder to get it you need:
    - the client token you got in step1
    - the temporary token got in step 2
    - the verifier you got in  step 3

In a shell do:

.. code-block:: tcsh
    :linenos:

    $ rim-resourcetoken --url=http://yourmagentoserver/oauth/token --clientkey=yourclientkey --clientsecret=yourclientsecret 
    --tmpkey=somecomplicatedtemporarykey --tmpsecret=somecomplicatedtemporarysecret --verifier=somecomplicatedverifier

Make sure you fill `clientkey`,  `clientsecret`, `tmpkey`, `tmpsecret` and `verifier` with the appropriates values. You should see something like this:

.. code-block:: tcsh
    :linenos:

    Obtained a resource token.
    Resource key: someresourcekey
    Resource secret: someresourcetoken

You completed the final step. Every script that uses Restinmagento will use the **client token** and the **resource token**. `Temporary token` and `verifier` are no longer needed.


