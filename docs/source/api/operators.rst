.. _operators_module:


Operators
=============================

This section provides API documentation for the :mod:`restinmagento.operators` objects. Every operator exposed here are meant to be used in a filter method.


.. autoclass:: restinmagento.operators.not_

.. autoclass:: restinmagento.operators.in_

.. autoclass:: restinmagento.operators.notin_

.. autoclass:: restinmagento.operators.gt_

.. autoclass:: restinmagento.operators.lt_

.. autoclass:: restinmagento.operators.range_
