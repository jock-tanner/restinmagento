.. _model_module:

.. py:module:: restinmagento.model


Model
=============================

This section provides API documentation for the :mod:`restinmagento.model` module.


.. autoclass:: Model
   :members:

   .. automethod:: __init__

   A model initialization requires specific data in order to be magento `compliant`.
    For example here is how you initialize a Product:

    .. code-block:: python
            :linenos:

            product_data = dict(type_id='simple', attribute_set_id='4',
                sku='prodsku', name='prodname', price='10',
                weight='1', status='1', visibility='2',
                tax_class_id='2', description='product description',
                short_description='short description')
        
            product = Product(data=product_data)

    You could initiliaze a Product with fewer data and the :meth:`__init__` method would not crash, 
    but these are the mandatory fields that a Magento server expects. Not using them would cause the :meth:`save` method to crash.
    For a complete reference see the 
    `Magento official documentation <http://www.magentocommerce.com/api/rest/Resources/resources.html>`_: 


   





   


