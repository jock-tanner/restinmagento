.. _collectionmanager_module:

.. py:module:: restinmagento.collectionmanager


Collectionmanager
=============================

This section provides API documentation for the :mod:`restinmagento.collectionmanager` module.


.. autoclass:: CollectionManager
   :members:


   .. automethod:: 


